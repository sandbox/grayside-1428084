Bot GitHub Integration
----------------------

This project provides an integration between GitHub and the Bot module. More
specifically it makes GitHub a Bot Project provider by utilizing the patch in
http://drupal.org/node/969294.

It has the following functionality:
 * Paste of a git commit url gets commit data.
 * Shorthand syntax allows request of commit and branch information.
   * Configurable prefix for the commit or branch name.
   * Per-channel default for the GitHub account/Organization namespace.
   * Per-channel default for the GitHub repository.
   * Ability to specify a repository to override the channel default.
 * Ability to specify user account & api key for authenticated GitHub requests.
   This allows the Bot to pull information about otherwise private repositories.
 * De-duplication and 5-item flood control per line.

A commit request might look like:
  Grayside: ~0a6fc5129b, %master
or
  Grayside: ~bot_github:0a6fc5129b

And produce:
  GitHubBot: Commit "Issue #1042: Create an Awesome GitHub Bot integration." by Grayside, http://github.com/Grayside/fakeproject/0a6fc5129b (47 IRC mentions)
  GitHubBot: Branch "master" last updated on Fri, 03 Feb 2012 15:03:31 -0800, https://github.com/GoingOn/noc/tree/master (2 IRC mentions)
